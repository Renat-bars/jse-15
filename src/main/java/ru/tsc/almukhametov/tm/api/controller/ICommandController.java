package ru.tsc.almukhametov.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showErrArg(String arg);

    void showErrCommand(String command);

    void exit();

}
