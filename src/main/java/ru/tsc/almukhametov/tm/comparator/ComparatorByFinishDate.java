package ru.tsc.almukhametov.tm.comparator;

import ru.tsc.almukhametov.tm.api.entity.IHasFinishDate;

import java.util.Comparator;

public class ComparatorByFinishDate implements Comparator<IHasFinishDate> {

    private static final ComparatorByFinishDate INSTANCE = new ComparatorByFinishDate();

    private ComparatorByFinishDate(){  }

    public static ComparatorByFinishDate getInstance() { return INSTANCE; }

    @Override
    public int compare(IHasFinishDate o1, IHasFinishDate o2) {
        if(o1 == null || o2 == null) return 0;
        return o1.getFinishDate().compareTo(o2.getFinishDate());
    }
}
