package ru.tsc.almukhametov.tm.component;

import ru.tsc.almukhametov.tm.api.controller.ICommandController;
import ru.tsc.almukhametov.tm.api.controller.IProjectController;
import ru.tsc.almukhametov.tm.api.controller.IProjectTaskController;
import ru.tsc.almukhametov.tm.api.controller.ITaskController;
import ru.tsc.almukhametov.tm.api.repository.ICommandRepository;
import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.api.service.ICommandService;
import ru.tsc.almukhametov.tm.api.service.IProjectService;
import ru.tsc.almukhametov.tm.api.service.IProjectTaskService;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.controller.CommandController;
import ru.tsc.almukhametov.tm.controller.ProjectController;
import ru.tsc.almukhametov.tm.controller.ProjectTaskController;
import ru.tsc.almukhametov.tm.controller.TaskController;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.repository.CommandRepository;
import ru.tsc.almukhametov.tm.repository.ProjectRepository;
import ru.tsc.almukhametov.tm.repository.TaskRepository;
import ru.tsc.almukhametov.tm.service.CommandService;
import ru.tsc.almukhametov.tm.service.ProjectService;
import ru.tsc.almukhametov.tm.service.ProjectTaskService;
import ru.tsc.almukhametov.tm.service.TaskService;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Scanner;

import static ru.tsc.almukhametov.tm.enumerated.Status.*;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void start(final String[] args) {
        System.out.println("** Welcome to THE REAL WORLD **");
        parseArgs(args);
        initData();
        process();
    }

    private void initData(){
        projectService.add(new Project("Drum & Bass", "Music genre")).setStatus(COMPLETED);
        projectService.add(new Project("Lexus", "Vehicles factory")).setStatus(Status.NOT_STARTED);
        projectService.add(new Project("FAW", "Vehicles factory")).setStatus(Status.NOT_STARTED);
        projectService.add(new Project("ROSATOM", "Nuclear plant")).setStatus(Status.COMPLETED);
        projectService.add(new Project("Sport", "Citius, Altius, Fortius")).setStatus(Status.IN_PROGRESS);
        taskService.add(new Task("Neurophunk", "podcast")).setStatus(Status.COMPLETED);
        taskService.add(new Task("IS500", "Sedan cars")).setStatus(Status.NOT_STARTED);
        taskService.add(new Task("NPP", "Balaklavskaya")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Hockey", "Tampa")).setStatus(Status.IN_PROGRESS);
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showErrArg(arg);
        }
    }

    public void parseCommands(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectTaskController.removeById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectTaskController.removeByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectTaskController.removeByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeProjectStatusByName();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeTaskStatusByName();
                break;
            case TerminalConst.TASK_LIST_BY_PROJECT:
                projectTaskController.findAllTaskByProjectId();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskByProjectId();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskByProjectId();
                break;
            default:
                commandController.showErrCommand(command);
        }
    }

    private void process() {
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                parseCommands(command);
            } catch (final Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

}
