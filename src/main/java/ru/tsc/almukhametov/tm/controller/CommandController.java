package ru.tsc.almukhametov.tm.controller;

import ru.tsc.almukhametov.tm.api.controller.ICommandController;
import ru.tsc.almukhametov.tm.api.service.ICommandService;
import ru.tsc.almukhametov.tm.exception.system.UnknownArgumentException;
import ru.tsc.almukhametov.tm.exception.system.UnknownCommandException;
import ru.tsc.almukhametov.tm.model.Command;
import ru.tsc.almukhametov.tm.util.UnitUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + UnitUtil.convertBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = UnitUtil.convertBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + UnitUtil.convertBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + UnitUtil.convertBytes(usedMemory));
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Renat Almukhametov");
        System.out.println("E-MAIL: rralmukhametov@tsconsulting.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    @Override
    public void showErrCommand(final String command) { throw new UnknownCommandException(command);}

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    public void showErrArg(final String arg) { throw new UnknownArgumentException(arg); }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
