package ru.tsc.almukhametov.tm.enumerated;

import ru.tsc.almukhametov.tm.comparator.*;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED ("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    DATE_FINISH("Sort by finish date", ComparatorByFinishDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {return displayName;}

    public Comparator getComparator() {return comparator;}

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }
}
