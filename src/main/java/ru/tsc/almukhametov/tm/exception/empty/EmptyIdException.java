package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException(){
        super("Error, Id is empty");
    }

    public EmptyIdException(String value) {
        super("Error, ``" + value + "`` id is empty");
    }

}
