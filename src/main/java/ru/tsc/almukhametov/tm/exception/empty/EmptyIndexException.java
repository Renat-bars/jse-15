package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException(){
        super("Error, index is empty");
    }

}
