package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException(){
        super("Error, name is empty");
    }

}
