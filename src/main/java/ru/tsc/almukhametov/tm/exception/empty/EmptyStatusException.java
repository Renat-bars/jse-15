package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException(){
        super("Error, status is empty");
    }

}
