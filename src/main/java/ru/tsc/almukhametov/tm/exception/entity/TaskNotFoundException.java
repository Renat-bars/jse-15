package ru.tsc.almukhametov.tm.exception.entity;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class TaskNotFoundException  extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
