package ru.tsc.almukhametov.tm.util;

import ru.tsc.almukhametov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws ArithmeticException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e){
            throw new IndexIncorrectException(value);
        }
    }

}
